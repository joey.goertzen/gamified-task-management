from django.test import TestCase
from .models import User
# Create your tests here.


class TestUser(TestCase):
    def setUp(self):
        User.objects.create(
            first_name="Michael",
            last_name="O'Connell",
            username="michael05",
        )
    
    def test_user_matches(self):
        user = User.objects.get(username="michael05")
        self.assertEquals(user, User.objects.get(id=1))
        self.assertEquals(user.spendable_points, 0)
        self.assertEquals(user.last_name, "O'Connell")