# Generated by Django 4.0.3 on 2023-02-23 19:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Tasks_App', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='completed_date',
            field=models.DateField(null=True),
        ),
    ]
