import "./CreateTask.css"

import { UserContext } from "../../Utils/UserContext"

import { useState, useEffect, useContext } from "react"
import { useNavigate } from "react-router"

import { fetchPriority } from "../../Fetches/PriorityFetch/PriorityFetch"
import { createFetch } from "../../Fetches/CreateFetch/CreateFetch"

const CreateTask = ({setUpdate, update}) => {
    const { userDetail } = useContext(UserContext)
    const navigate = useNavigate()
    const userId = userDetail?.User?.id

    
    // mimic the status instances but without the completed
    const [statusList, ] = useState([
		{"status_type": "To-Do",
			"id": 1},
		{"status_type": "In-Progress",
			"id": 2}
	])
    const [priorityList, setPriorityList] = useState([])

    useEffect(() => {
        async function getPriorityList() {
            const listOfPriorities = await fetchPriority()
            setPriorityList(listOfPriorities)
        }

        getPriorityList()
    },[])

    // values used to create task
    const [createStatus, setCreateStatus] = useState("")
    const [createPriority, setCreatePriority] = useState("")
    const [createTitle, setCreateTitle] = useState("")
    const [createDescription, setCreateDescription] = useState("")
    const [createDueDate, setCreateDueDate] = useState("")

    const handleStatus = (event) => {
        setCreateStatus(event.target.value)
    }

    const handlePriority = (event) => {
        setCreatePriority(event.target.value)
    }

    const handleTitle = (event) => {
        setCreateTitle(event.target.value)
    }

    const handleDate = (event) => {
        setCreateDueDate(event.target.value)
    }

    const handleDescription = (event) => {
        setCreateDescription(event.target.value)
    }

    const handleSubmit = async (event) => {
        const content = {
            "title": createTitle,
            "user": userId,
            "status": createStatus,
            "priority": createPriority,
            "due_date": createDueDate,
            "description": createDescription
        }

        const response = await createFetch(content)

        if (response.ok) {
            navigate("/")
        } else {
            alert("Unable to create task make sure all fields are filed and title is not too long.")
        }
        
    }

    return (
        <div className="create-task-container">
        <h4>Create Task:</h4>
            <div>
                <label>Title: </label>
                <input type="text" onChange={handleTitle} required/>
            </div>
            <div>
                <label>Description: </label>
                <input type="text" onChange={handleDescription} required/>
            </div>
            <div>
                <select onChange={handleStatus}>
                    <option value="">Select a Status</option>
                        {statusList.map(statusItem => {
                            return (
                                <option key={statusItem.id} value={statusItem.id}>
                                    {statusItem.status_type}
                                </option>
                            )
                        })}
                </select>
            </div>
            <div>
                <select onChange={handlePriority}>
                    <option value="">Select a Priority</option>
                        {priorityList.map(priorityItem => {
                            return (
                                <option key={priorityItem.id} value={priorityItem.id}>
                                    {priorityItem.priority_type}
                                </option>
                            )
                        })}
                </select>
            </div>
            <div>
                <label>Due Date: </label>
                <input type="date" onChange={handleDate} />
            </div>
            <button className="create-task-submit" onClick={handleSubmit}>Submit</button>
        </div>
    )
}

export default CreateTask