import "./TaskDetail.css"



import { useState, useEffect } from "react"
import { useLocation, useNavigate } from "react-router"

import { taskDetailFetch } from "../../Fetches/Task Detail Fetches/taskDetailFetch"

const TaskDetail = () => {
    const [task, setTask] = useState({})
    const navigate = useNavigate()
    const location = useLocation()
    const taskId = location.state.taskId

    useEffect(() => {
        async function getTask() {
            const taskInfo = await taskDetailFetch(taskId)
            setTask(taskInfo)
        }


        if (taskId !== undefined) {
            getTask()
        }
    },[taskId])

    
    const handleUpdate = () => {
        navigate(`update/`, {state: {"task" :task}})
    }

    return (
        <div className="task-detail-container">
            <div className="task-detail-title">
                Title: {task.title}
            </div>
            <button className="update-button" onClick={handleUpdate}>Update</button>
            <div className="task-detail-description-title">
                Description:
            </div>
            <div className="task-detail-description">
                {task.description}
            </div>
            <div>
                Status of Task: {task?.status?.status_type}
            </div>
            <div>
                Task Priority: {task?.priority?.priority_type}
            </div>
            <div>
                Due Date: {task.due_date}
            </div>
            <div>
                
            </div>

        </div>
    )
}

export default TaskDetail