import "./UnreadNotifications.css"

import { useContext, useEffect, useState } from "react"
import { UserContext } from "../../Utils/UserContext"

import { fetchUnreadUserNotifications } from "../../Fetches/NotificationFetches/fetchUnreadUserNotifications"
import { fetchMarkNotificationRead } from "../../Fetches/NotificationFetches/fetchMarkNotificationRead"

const UnreadNotifications = ({update, setUpdate}) => {
    const { userDetail } = useContext(UserContext)
    const userId = userDetail?.User?.id
    const [unreadNotifications, setUnreadNotifications] = useState([])

    useEffect(() => {
        async function getUnreadUserNotifications() {
            if (userId !== undefined) {
                const responseUnreadNotifications = await fetchUnreadUserNotifications(userId)
                setUnreadNotifications(responseUnreadNotifications)
            }
        }
        getUnreadUserNotifications()
    }, [userId, update])

    const handleMarkRead = async (notificationId) => {
        const response = await fetchMarkNotificationRead(notificationId)
        if (response.ok) {
            setUpdate(!update)
        }
    }


    return (
        <div className="notifications-container">  
            {unreadNotifications.map((notification) => {
                return (
                    <div key={notification.id} className="outer-notification-container">
                        <div className="notification-container">

                            {notification.message}
                        </div>
                        <button className="read-button" onClick={() => handleMarkRead(notification.id)}>Mark Read</button>
                    </div>
                )
            })}

        </div>
    )
}

export default UnreadNotifications