import "./UserPage.css"

import { useEffect, useState } from "react"
import { useParams } from "react-router"

import { fetchUserInfo } from "../../Fetches/User Fetches/FetchUserInfo"

const UserPage = () => {
    const {username} = useParams()
    const [rewards, setRewards] = useState([])
    const [displayUsername, setDisplayUsername] = useState("")
    const [spendablePoints, setSpendablePoints] = useState(0)
    const [lifetimePoints, setLifetimePoints] = useState(0)

    useEffect(() => {
        async function getUserData() {
            if (username !== undefined) {
                const response = await fetchUserInfo(username)
                setRewards(response.User.rewards)
                setDisplayUsername(response.User.username)
                setLifetimePoints(response.User.lifetime_points)
                setSpendablePoints(response.User.spendable_points)
                
            }
        }
        getUserData()
    }, [username])

    return (
        <div className="user-page-container">
        <div className="user-information">
            <h4 className="title-user-rewards">User Information:</h4>
            <p>Username: {displayUsername}</p>
            <p>Spendable Points: {spendablePoints}</p>
            <p>Lifetime Points: {lifetimePoints}</p>
        </div> 
            <h3>My Rewards:</h3>
            <div className="rewards-container">
                {rewards.map((reward) => {
                    return (
                        <div key={reward.pk} className="reward-container">
                            <img src={reward.picture} alt={reward.name} className="reward-image-container"></img>
                            <h4>{reward.name}</h4>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default UserPage