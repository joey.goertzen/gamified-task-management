import "./UpdateTask.css"

import { useEffect, useState, useContext } from "react"

import { useLocation, useNavigate } from "react-router"
import { UserContext } from "../../Utils/UserContext"

import { updateTaskFetch } from "../../Fetches/UpdateTask/UpdateTaskFetch"
import { fetchPriority } from "../../Fetches/PriorityFetch/PriorityFetch"
import { fetchStatuses } from "../../Fetches/Status Fetch/StatusFetch"
import { fetchMarkTaskComplete } from "../../Fetches/Task List Fetches/MarkComplete"

const UpdateTask = ({setUpdate, update}) => {
    const location = useLocation()
    const navigate = useNavigate()
    const [priorityTypes, setPriorityTypes] = useState([])
    const [statusTypes, setStatusTypes] = useState([])
    const task = location?.state?.task
    const { userDetail } = useContext(UserContext)

    const userId = userDetail?.User?.id

    const [updateStatus, setUpdateStatus] = useState(task.status.id)
    const [updatePriority, setUpdatePriority] = useState(task.priority.id)
    const [updateTitle, setUpdateTitle] = useState(task.title)
    const [updateDescription, setUpdateDescription] = useState(task.description)
    const [updateDueDate, setUpdateDueDate] = useState(task.due_date)

    useEffect(() => {
        async function getStatusesPriorities() {
            const listOfPriorities = await fetchPriority()
            setPriorityTypes(listOfPriorities)
            const listOfStatuses = await fetchStatuses()
            setStatusTypes(listOfStatuses)
        }
        getStatusesPriorities()
    },[])

    const handleStatus = (event) => {
        setUpdateStatus(event.target.value)
    }

    const handlePriority = (event) => {
        setUpdatePriority(event.target.value)
    }

    const handleTitle = (event) => {
        setUpdateTitle(event.target.value)
    }

    const handleDate = (event) => {
        setUpdateDueDate(event.target.value)
    }

    const handleDescription = (event) => {
        setUpdateDescription(event.target.value)
    }

    const handleSubmit = async (event) => {
        const content = {
            "title": updateTitle,
            "user": userId,
            "status": updateStatus,
            "priority": updatePriority,
            "due_date": updateDueDate,
            "description": updateDescription
        }


        const response = await updateTaskFetch(task.id, content)
        const updateResponse = await response.json()

        // variable used if the status was marked as complete and the fetch failed because it was already complete
        let failed = false

        if (updateResponse.Task === "Can not update the task") {

            failed = true
            alert("Not able to update completed or deleted tasks")
        }

        // if user marked status as complete and the update did not return a failed because it has already been
        // completed the mark complete fetch will run completing the task and assigning points to the user
        if (updateStatus === "3" && !failed) {
            const completeResponse = await fetchMarkTaskComplete(task.id)
            
            if (response.ok && completeResponse.ok) {
                setUpdate(!update)
                navigate("/")
            }
        }
        if (response.ok && !failed) {
            navigate("/")
        }
        
    }

    return (
        <div className="create-task-container">
        <h4>Update Task: {task.title}</h4>
            <div>
                <label>Title: </label>
                <input type="text" value={updateTitle} onChange={handleTitle} required/>
            </div>
            <div>
                <label>Description: </label>
                <input type="text" value={task.description} onChange={handleDescription} required/>
            </div>
            <div>
                <select onChange={handleStatus}>
                    <option value={updateStatus}>{task.status.status_type}</option>
                        {statusTypes.map(statusItem => {
                            return (
                                <option key={statusItem.id} value={statusItem.id}>
                                    {statusItem.status_type}
                                </option>
                            )
                        })}
                </select>
            </div>
            <div>
                <select onChange={handlePriority}>
                    <option value={updatePriority}>{task.priority.priority_type}</option>
                        {priorityTypes.map(priorityItem => {
                            return (
                                <option key={priorityItem.id} value={priorityItem.id}>
                                    {priorityItem.priority_type}
                                </option>
                            )
                        })}
                </select>
            </div>
            <div>
                <label>Due Date: </label>
                <input type="date" value={updateDueDate} onChange={handleDate} />
            </div>
            <button className="create-task-submit" onClick={handleSubmit}>Submit</button>
        
        </div>
    )
}

export default UpdateTask