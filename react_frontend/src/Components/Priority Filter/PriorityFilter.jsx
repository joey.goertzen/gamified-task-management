import "./PriorityFilter.css"

const PriorityFilter = ({handlePriority1, handlePriority2, handlePriority3, priority1, priority2, priority3}) => {

    return (
        <div className="priority-filter-container">
        <h4>Filter by priority:</h4>
            <div className="priority-filter-to-do">
                <label className="checkbox-label">Filter by Urgent:</label>
                <input className="checkbox-input" type="checkbox" checked={priority1} onChange={handlePriority1} />
            </div>
            <div className="priority-filter-in-progress">
                <label className="checkbox-label">Filter by Important:</label>
                <input className="checkbox-input" type="checkbox" checked={priority2} onChange={handlePriority2} />
            </div>
            <div className="priority-filter-in-progress">
                <label className="checkbox-label">Filter by Normal:</label>
                <input className="checkbox-input" type="checkbox" checked={priority3} onChange={handlePriority3} />
            </div>
        </div>
    )
}

export default PriorityFilter