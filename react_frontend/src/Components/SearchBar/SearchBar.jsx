import "./SearchBar.css"

const SearchBar = ({handleSearchBar}) => {
    return (
        <div className="search-container">
            <h4>Filter by Search:</h4>
            <div className="search-bar">
                <label>Search Tasks:  </label>
                <input type="text" id="search" onChange={handleSearchBar} aria-label="Search" />
            </div>
        </div>
        )
}

export default SearchBar