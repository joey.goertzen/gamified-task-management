import "./MobileTasks.css"

import { useNavigate } from "react-router"

const MobileTasks = ({mobile, tasks}) => {
    const navigate = useNavigate()
    if (!mobile) {
        return null
    }


    const handleNavigateTask = (taskId) => {
        navigate(`taskdetail/`, {state: {"taskId" :taskId}})
    }

    return (
        <div>
            {tasks.map((task) => {
                return (
                    <div key={task.id}>
                    <div className="mobile-task-container" onClick={() => handleNavigateTask(task.id)}>
                        <h4>Title: {task.title}</h4>
                        <div className="mobile-task-all-properties-container">
                            <div className="mobile-task-description-title">
                                Description:
                            </div>
                            <div className="mobile-task-description-body">
                                {task.description}
                            </div>
                            <div className="mobile-task-properties-container">
                                <div className="mobile-task-due-date-title">
                                    Due Date:
                                </div>
                                <div className="mobile-task-due-date-date">
                                    {task.due_date}
                                </div>
                                <div className="mobile-task-status-title">
                                    Status:
                                </div>
                                <div className="mobile-task-status-status">
                                    {task.status.status_type}
                                </div>
                                <div className="mobile-task-priority-title">
                                    Priority:
                                </div>
                                <div className="mobile-task-priority-priority">
                                    {task.priority.priority_type}
                                </div>
                            </div>
                        </div>

                    </div>
                    </div>
                )
            })}
        </div>
    )
}

export default MobileTasks