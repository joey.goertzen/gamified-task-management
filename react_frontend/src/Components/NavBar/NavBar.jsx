import "./NavBar.css"

import { NavLink } from "react-router-dom";
import { useContext, useEffect, useState } from "react";

import { Outlet } from "react-router-dom";

import { useAuthorization } from "../../Utils/Authorization";
import { UserContext } from "../../Utils/UserContext";
import { fetchUnreadNotificationCount } from "../../Fetches/NotificationFetches/fetchUnreadNotificationCount";

const MINUTE_MS = 60000

function NavBar ({setUpdate, update}) {
    const [logout] = useAuthorization();
    const {userDetail} = useContext(UserContext)
    const username = userDetail?.User?.username
    const userId = userDetail?.User?.id
    const [unreadNotificationCount, setUnreadNotificationCount] = useState(0)

    const getNotificationCountUpdate = async (userId) => {
        if (userId !== undefined) {
            const count = await fetchUnreadNotificationCount(userId)
            setUnreadNotificationCount(count)
        }
    }  

    useEffect(() => {
        // this function will call immediately because the below call of getNotificationCountUpdate will wait a minute to fire for the first time
        async function getNotificationCount() {

            await getNotificationCountUpdate(userId)
        } 
        getNotificationCount()

        // this sets an interval and call the count function every minute, this is so updated notification data from the backend is included
        const interval = setInterval( async () => {
            await getNotificationCountUpdate(userId)
        }, MINUTE_MS)
        return () => clearInterval(interval)
    }, [userId, update])

    async function handleClick(){
        await logout()
        await setUpdate(!update)
        
    }

    return (
        <div style={{"marginBottom": "1rem"}}>
        <header>
        <nav className="navbar navbar-expand-lg navbar-light bg-light ">
        <div className="container-fluid ">
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">        

                {username ?
                <>
                <li className="nav-item">
                    <NavLink className="nav-link" aria-current="page" to="/">My Tasks</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link hover-effect" aria-current="page" to={`/user/${username}`}>My Page</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" aria-current="page" to="/leaderboard">Leader Board</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" aria-current="page" to="/shop">Reward Shop</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" aria-current="page" to="/createtask">Create Task</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" aria-current="page" to="/notifications">Unread Notifications: {unreadNotificationCount}</NavLink>
                </li> 
                <li className="nav-item" onClick={handleClick}>
                    <NavLink  className="nav-link"  aria-current="page"to="/login">Log Out</NavLink>
                </li>
                </>

                :
                
                <li className="nav-item">
                    <NavLink  className="nav-link"  aria-current="page" to="/login">Log In</NavLink>
                </li>
                }
                
            </ul>
        </div>
        </div>
        </nav>
    </header>
    <Outlet />
    </div>
    )
}

export default NavBar
