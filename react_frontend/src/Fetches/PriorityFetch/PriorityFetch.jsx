export const fetchPriority = async () => {
    const prioritiesUrl = `${process.env.REACT_APP_TASKS}tasks/priority_list/`
    const prioritiesConfig = {
        method: "get"
    }
    const response = await fetch(prioritiesUrl, prioritiesConfig)
    const jsonResponse = await response.json()
    const prioritiesList = jsonResponse["Priority_list"]
    return prioritiesList
}