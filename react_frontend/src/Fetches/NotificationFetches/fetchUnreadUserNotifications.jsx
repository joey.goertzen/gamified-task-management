export const fetchUnreadUserNotifications = async (userId) => {
    const url = `${process.env.REACT_APP_USERS}users/notifications/user_unread/${userId}`
    const fetchConfig = {
        method: "get"
    }
    const response = await fetch(url, fetchConfig)
    const unJsonResponse = await response.json()
    const notifications = unJsonResponse["Notifications"]
    return notifications
}