export const fetchMarkNotificationRead = async (notificationId) => {
    const url = `${process.env.REACT_APP_USERS}users/notifications/mark_complete/${notificationId}/`
    const fetchConfig = {
        method: "put"
    }
    const response = await fetch(url, fetchConfig)
    return response
}