

export const updateTaskFetch = async (taskId, taskInfo) => {
    const updateUrl = `${process.env.REACT_APP_TASKS}tasks/update_task/${taskId}/`
    const updateConfig = {
        method: "put",
        body: JSON.stringify(taskInfo),
        headers: {
            "Content-Type": "application/json",
        },
    }
    const response = await fetch(updateUrl, updateConfig)

    return response
}