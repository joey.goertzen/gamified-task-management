export const fetchTaskList = async (taskUrl) => {
    const fetchConfigTasks = {
        method: "get"
    }

    const response = await fetch(taskUrl, fetchConfigTasks)
    const taskObject = await response.json()
    const taskList = taskObject["Tasks"]
    return taskList
}