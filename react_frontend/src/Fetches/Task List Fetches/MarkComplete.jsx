export const fetchMarkTaskComplete = async  (taskId) => {
    const fetchConfig = {
        method: "put"
    }

    const fetchUrl = `${process.env.REACT_APP_TASKS}tasks/mark_complete/${taskId}/`
    const response = await fetch(fetchUrl, fetchConfig)

    return response
}