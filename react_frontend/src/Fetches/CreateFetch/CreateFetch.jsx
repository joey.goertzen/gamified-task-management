export const createFetch = async (content) => {
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(content),
        headers: {
            "Content-Type": "application/json"
        }
    }
    const fetchUrl = `${process.env.REACT_APP_TASKS}tasks/task_list/`
    const response = await fetch(fetchUrl, fetchConfig)
    return response
}