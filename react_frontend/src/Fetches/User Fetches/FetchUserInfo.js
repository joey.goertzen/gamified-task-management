export async function fetchUserInfo(username) {
    const response = await fetch(`${process.env.REACT_APP_USERS}users/individual_user/${username}/`)
    const userDetail = await response.json()
    return userDetail
}